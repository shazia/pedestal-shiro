# pedestal-shiro

A [pedestal](https://github.com/pedestal/pedestal) and [pocheshiro](https://github.com/inventiLT/Pocheshiro) integration. 

## Getting Started
Add the necessary dependency to your Leningen project.clj and require the library in your namespace.

```clojure
[tawus/pedestal-shiro "0.1.0"]
```

```clojure
(ns my-ns 
  (:require [pedestal-shiro.core :as pedestal-shiro])
```

### Usage
Once you have created a pedestal app and setup a realm of your choice using [pocheshiro](https://github.com/inventiLT/Pocheshiro) you can configure the shiro interceptor using `pedestal-shiro.core/security` interceptor.

```clojure
(ns package-name.service
  (:import [org.apache.shiro.web.mgt DefaultWebSecurityManager])
  (:require [pedestal-shiro.core :as pedestal-shiro]
            [pocheshiro.core :as shiro]))

(def security-manager-retriever
   (constantly (DefaultWebSecurityManager. my-realm)))    

(pedestal-shiro/security
  {:security-manager-retriever security-manager-retriever
   :login-page :login :access-denied-page :access-denied})
```

`:login` and `:access-denied` are routes to pages you want to show in case of authentication and authorization failure respectively.

For protecting the urls we use `pedestal-shiro.core/guard` interceptor.

```clojure
(defroutes routes                                                                                                                                                                 
  [[["/" {:get home-page}                                                                                                                                                         
     ;; Set default interceptors for /about and any other paths under /                                                                                                           
     ^:interceptors [session-interceptor                                                                                                                                          
                     (pedestal-shiro/security                                                                                                                                     
                      {:security-manager-retriever security-manager-retriever                                                                                                     
                       :login-page :login :access-denied-page :access-denied})                                                                                                    
                     (body-params/body-params) bootstrap/html-body]                                                                                                               
     ["/public"   {:get public-page}]                                                                                                                                             
     ["/secure"   {:get secure-page}                                                                                                                                              
      ^:interceptors [(pedestal-shiro/guard shiro/authenticated)]]                                                                                                                
     ["/members-only" {:get members-only}                                                                                                                                         
      ^:interceptors [(pedestal-shiro/guard (shiro/authorized {:roles #{:member}}))]]                                                                                             
     ["/members-with-write" {:get members-with-write-only}                                                                                                                        
      ^:interceptors [(pedestal-shiro/guard                                                                                                                                       
                       (shiro/and* (shiro/authorized {:roles #{:member}})                                                                                                         
                                   (shiro/authorized {:permissions #{:write}})))]]                                                                                                
     ["/logout"   {:get do-logout}]                                                                                                                                               
     ["/do-login" {:get do-login}]                                                                                                                                                
     ["/access-denied" {:get [:access-denied access-denied]}]                                                                                                                     
     ["/login"    {:get [:login login]}]]]])
```

### Example
An example can be found in the [tests directory](https://bitbucket.org/tawus/pedestal-shiro/src/master/test/pedestal_shiro/)

## License
Copyright © 2015 _Taha Hafeez Siddiqi_. 

Distributed under the Eclipse Public License, the same as Clojure.