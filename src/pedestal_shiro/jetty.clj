(ns pedestal-shiro.jetty
  (:import [org.eclipse.jetty.server.session SessionHandler]
           [org.eclipse.jetty.servlet ServletContextHandler])
  (:require [io.pedestal.http :as server]))

(defn session-handler
  [service-map]
  (letfn [(set-handler [context]
            (.setSessionHandler ^ServletContextHandler context (SessionHandler.)))]
    (-> service-map
        (assoc-in [::server/container-options :context-configurator] set-handler)
        (assoc-in [::server/jetty-options :context-configurator] set-handler))))
